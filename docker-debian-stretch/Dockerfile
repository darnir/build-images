FROM debian:stretch

LABEL maintainer "Tim Rühsen <tim.ruehsen@gmx.de>"

WORKDIR /builds/common

RUN apt-get update && apt-get install -y \
	pkg-config \
	gcc \
	g++ \
	make \
	gettext \
	autopoint \
	git \
	autoconf \
	automake \
	libtool \
	texinfo \
	flex \
	gperf \
	python \
	libasan3 \
	libubsan0 \
	libtsan0 \
	liblzma-dev \
	libidn2-0-dev \
	libunistring-dev \
	zlib1g-dev \
	libbz2-dev \
	gnutls-dev \
	libpsl-dev \
	libnghttp2-dev \
	libmicrohttpd-dev \
	libpcre2-dev \
	lcov \
	doxygen \
	openssl \
	libssl-dev \
	texlive \
	valgrind \
	lzip \
	graphviz \
	clang \
	wget \
	cppcheck \
	flawfinder \
	pandoc \
	libgpgme-dev \
	ccache

RUN git clone https://github.com/bagder/libbrotli
RUN cd libbrotli && ./autogen.sh && ./configure
RUN cd libbrotli && make && make install

RUN git clone --recursive https://git.savannah.gnu.org/git/gnulib.git
ENV GNULIB_SRCDIR /builds/common/gnulib

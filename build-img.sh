#!/bin/sh

image=$1
startdir=$2
registry=$3
project=$4

master_build=0
if test "$CI_PROJECT_NAMESPACE" = "gnuwget" && test "$CI_PROJECT_NAME" = "build-images"; then
	master_build=1
fi

if test $master_build = 0; then
	echo Image is already up-to-date
	exit 0
fi

set -e
docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $registry
docker build -t $registry/$project:$image $startdir
docker push $registry/$project:$image

exit 0

#!/bin/bash

# Helper script to download and install AUR packages

PKGNAME="$1"

pushd /tmp
sudo -u builduser git clone "https://aur.archlinux.org/${PKGNAME}.git"
pushd "$PKGNAME"
yes | sudo -u builduser makepkg -s
popd
popd
